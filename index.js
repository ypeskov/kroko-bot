"use strict";

let listeners = require('./src/modules/event_listeners');
let client    = require('./src/irc_client');

client.addListener('message', listeners.onMessage);
client.addListener('registered', listeners.onRegistered);
client.addListener('motd', listeners.onMOTD);
client.addListener('join', listeners.onJoin);
client.addListener('raw', listeners.onRaw);
client.addListener('names', listeners.getNamesList);
client.addListener('quit', listeners.quit);
client.addListener('nick', listeners.nickChanged);
client.addListener('actionnick', listeners.onAction);
client.addListener('names', listeners.onNames);


// Run the bot at last!
client.connect();