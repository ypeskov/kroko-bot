let irc = require('irc-fork-non-latin-nicks');
let config = require('./config');

let client = new irc.Client("irc.lucky.net", "krokobot", config);

client.opt.sendEmailAddress = config.sendEmailAddress;

module.exports = client;