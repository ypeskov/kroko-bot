"use strict";

require('dotenv').config();

const channels = process.env.channels.split(',');

let debug = false;
if (process.env.debug === 'true') {
    debug = true;
}

let autoConnect = false;
if (process.env.autoConnect === 'true') {
    autoConnect = true;
}

module.exports = {
    nick: process.env.nick || 'krokobot',
    port: process.env.irc_port || 6667,
    server: "irc.odessa.ua",
    debug: debug,
    autoConnect: autoConnect,
    millisecondsOfSilenceBeforePingSent: 60 * 1000,
    millisecondsBeforePingTimeout: 60 * 1000,
    channels: channels,
    sendEmailAddress: 'krokobot@peskov.info',

    database: {
        host: process.env.db_host || 'localhost',
        database: process.env.db_database || 'solanka',
        user: process.env.db_user || 'solanka',
        password: process.env.db_password || 'solanka'
    },

    silenceTime: process.env.slinceTime || 60000
};