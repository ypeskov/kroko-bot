"use strict";

const mysqlConnection = require('./mysql_connect');
const moment = require('moment');

let storage = {
    con: mysqlConnection,

    table: 'last_seen_users',

    updateRecord(nick, action, host, channel) {
        const now = moment().format('YYYY-MM-DD HH:mm:ss');

        nick = nick.toLowerCase();

        this.con.query(`SELECT * FROM ${this.table} WHERE nick = "${nick}"`, (err, result) => {
            if (err) throw new Error(err);

            let sql = '';
            if (result.length > 0) {
                sql = `UPDATE ${this.table} SET last_seen = "${now}", channel = "${channel}",`;
                sql += `host = "${host}", action = "${action}" WHERE id = ${result[0].id}`;

                this.con.query(sql, (err, result) => {});
            } else {
                sql = `INSERT INTO ${this.table} (nick, last_seen, channel, action, host)`;
                sql += `VALUES("${nick}", "${now}", "${channel}", "${action}", "${host}")`;

                this.con.query(sql, (err, result) => {});
            }
        });
    },

    lastSeen(nick, callback) {
        const queryNick = nick.toLowerCase();

        const sql = `SELECT * FROM ${this.table} WHERE nick = "${queryNick}"`;

        this.con.query(sql, (err, result) => {
            if (err) throw new Error(err);

            let text = '';
            if (result.length > 0) {
                let action = 'joining';
                if (result[0].action === 'PART' || result[0].action === 'QUIT') {
                    action = 'quiting';
                }

                const date = moment(result[0].last_seen).format('YYYY-MM-DD HH:mm:ss');

                text = `I saw ${nick} ${action} ${result[0].channel} on ${date}`;
            } else {
                text = `no idea about ${nick}`;
            }

            callback(text);
        });
    }
};

module.exports = storage;
