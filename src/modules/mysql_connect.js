"use strict";

const mysql = require('mysql');
const config = require('../config');

const connection = mysql.createConnection({
    connectionLimit: 5,
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database
});

connection.connect(err => {
    if (err) throw new Error(err);
});

module.exports = connection;