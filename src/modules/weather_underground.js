"use strict";

/**
 * Created by Yuriy Peskov<yuriy.peskov@gmail.com> on 2017-03-21.
 */

let http    = require('http');
let client  = require('../irc_client');

let weatherSrc = {};

weatherSrc.options = {
  host: "api.wunderground.com",
  path: "/api/b0b3c5716288f451/forecast/lang:RU/q/{{country}}/{{city}}.json",
  defaultCountry: "UA"
};

function getForecastStringFromJSON(forecast, callback) {
  let forecastStr = "";

  forecast.txt_forecast.forecastday.forEach((val, ind, arr) => {
    forecastStr += `${val.title} : ${val.fcttext_metric}\n`;
  });

  callback(forecastStr);

  return forecastStr;
}

weatherSrc.getWeather = function (params, outerCallback) {
  "use strict";

  let _self = this;

  let callback = (response) => {
    let str = "";

    /**
     * concatinate all data of response
     */
    response.on('data', chunk => {
      str += chunk;
    });

    response.on('end', function() {
      try {
        // let forecast = JSON.parse(str).forecast.simpleforecast.forecastday;
        let forecast = JSON.parse(str).forecast;
        getForecastStringFromJSON(forecast, outerCallback);
      } catch(e) {
        outerCallback(`${params.recipient}, не могу получить данные для ${params.city} in ${params.country}`);
      }
    });
  };

  let options = {
    host: _self.options.host,
    path: _self.options.path
      .replace("{{city}}", params.city)
      .replace("{{country}}", params.country)
  };

  http.get(options, callback);
};

/**
 *
 * @param {{string}} city
 * @returns {string | *}
 */
weatherSrc.processCityAliases = function(city) {
  city = city.toLowerCase();

  switch (city) {
    case 'николаев':
    case 'niko':
    case 'нико':
    case 'mykolayv':
      city = 'mykolaiv';
      break;

    case 'одесса':
    case 'одеса':
      city = 'odessa';
      break;

    case 'краков':
    case 'krakow':
      city = 'Kleparz';
      break;

    case 'киев':
    case 'київ':
    case 'kiev':
      city = 'kyiv';
      break;

    default:
      break;
  }

  return city;
};

weatherSrc.getCountryForCity = function(city) {
    let countryMap = {
        'krakow': 'poland',
        'Kleparz': 'poland',
    };

    return countryMap[city] === undefined ? null : countryMap[city];
};

/**
 *
 * @param {{String}} from
 * @param {{String}} to
 * @param {{String}} msg
 * @param {{String}} weatherCommand
 * @returns {{Object}}
 */
weatherSrc.getParametersFromMsg = function(from , to, msg, weatherCommand) {
  let _self = this;
  let msgParts = msg.trim().split(/\s+/);
  let city = null;
  let country = _self.options.defaultCountry;
  let parameters = {};
  let recipient = (to !== client.nick) ? to : from;

  switch ( msgParts.length ) {
    case 1:
      let responseMsg = `${from}, format: ${weatherCommand} [city] [in] [country]`;
      client.say(recipient, responseMsg);
      break;
    case 2:
      city = msgParts[1];
      break;
    case 3:
      if ( msgParts[2].toLowerCase() !== 'in' ) {
        city    = msgParts[1];
        country = msgParts[2];
      }
      break;
    case 4:
      city    = msgParts[1];
      country = msgParts[3];
      break;
    default:
      break;
  }

  parameters.city     = this.processCityAliases(city);

  parameters.country = this.getCountryForCity(parameters.city);
  if (parameters.country === null) {
      parameters.country = country;
  }

  parameters.recipient= recipient;

  return parameters;
};

module.exports = weatherSrc;
