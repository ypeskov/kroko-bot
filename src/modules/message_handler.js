"use strict";

const client = require('../irc_client');
const answers = require('./mentioned_answers');
const math = require('mathjs');
const decoder = require('@ypeskov/cyrillic_decoder');
const weather = require('./weather_underground');
const exchanger = require('./currency_exchanger');
const channelLogger = require('./channel_logger');
const fs = require('fs');
const seen = require('./last_seen');
const images = require('./images');
const interbank = require('./interbank_rate');
const todo = require('./todo');
const config = require('../config');

exchanger.init();
channelLogger.init();
interbank.init();

/**
 * Verifies whether my own nick is mentined in the message
 *
 * @param {String} nick
 * @param {String} msg
 * @returns boolean
 */
function isNickMentioned(nick, msg) {
    let found = msg.indexOf(nick);
    return found >= 0;
}

function isURLMentioned(msg) {
    return /http/.test(msg);
}

function isCommandMentioned(msg) {
    let commands = messageHandler.commands;
    let isMentioned = false;

    Object.keys(commands).some(val => {
        if (msg.indexOf(commands[val]) === 0) {
            isMentioned = true;
            return true;
        }
    });

    return isMentioned;
}

/**
 * return a random answer from generalAnswers
 *
 * @returns
 */
function getRandomAnswer() {
    let randomIndex = Math.floor(Math.random() * answers.generalAnswers.length);

    return answers.generalAnswers[randomIndex]
}

/**
 * checks whether latin and cyr symbols are in the same string
 */
function hasLatinAndCyrillyc(str) {
    return /[а-яА-Я]+/.test(str) && /[a-zA-Z]+/.test(str);
}

/**
 * if conditions are met than decode string to cyrillic and say back to channel/nick
 *
 * @param {String} from
 * @param {String} to
 * @param {String} msg
 */
function decodeIfRequired(from, to, msg) {

    //if channel is in silence mode then no decoding should be done
    if (to in messageHandler.isDecoding) {
        return;
    }

    if (to !== client.nick) {
        let hasNick = false;

        let usersNicks = Object.keys(client.chans[to].users);

        usersNicks.some((nick, index, arr) => {
            if (isNickMentioned(nick, msg)) {
                hasNick = true;
                return true;
            }
        });

        //do not decode messages with users nicks
        if (hasNick) {
            return false;
        }

        //do not decode strings with URL
        if (isURLMentioned(msg)) {
            return false;
        }

        //do not decode messages with commands
        if (isCommandMentioned(msg)) {
            return false;
        }

        if (hasLatinAndCyrillyc(msg)) {
            return false;
        }

        if (/[a-zA-Z]{5,}/.test(msg)) {
            let decodedStr = decoder.decode(msg);

            //say to private or to channel
            if (to !== client.nick) {
                client.say(to, decodedStr);
                channelLogger.log(client.nick, to, decodedStr);
            } else {
                client.say(from, decodedStr);
                channelLogger.log(client.nick, from, decodedStr);
            }
        }
    }
}

function sayWeatherForecastIfRequired(from, to, msg) {
    let weatherCommand = messageHandler.commands.weather;
    let isWeatherRequested = msg.indexOf(weatherCommand) === 0;

    if (isWeatherRequested) {
        let params = weather.getParametersFromMsg(from, to, msg, messageHandler.commands.weather);

        if (params.city && params.recipient) {
            weather.getWeather(params, (forecast) => {
                client.say(params.recipient, forecast);
                channelLogger.log(client.nick, to, forecast);
            });
        }
    }
}

let messageHandler = {};

messageHandler.commands = {
    'weather': '!погода',
    'rate': '!обмен',
    'help': '!помощь',
    'help2': '!help',
    'search': '!поиск',
    'log': '!лог',
    'lastSeen': '!lastseen',
    'mezhbank': '!межбанк',
    'todo': '!todo',
    'silence': '!тишина'
};

messageHandler.isDecoding = {};

messageHandler.errorMsg = function(nick, to, error) {
    const errorMsg = nick + ', якась хуйня трапилась';

    client.say(to, errorMsg);
    channelLogger.log(client.nick, to, errorMsg);

    fs.appendFile('errors.log', error + "\n", (err) => {
        if (err) {
            console.log(err);
        }
    });
};

/**
 * the main entry point for processing message
 */
messageHandler.coreProcess = function (from, to, msg) {
    let _self = this;

    channelLogger.log(from, to, msg);

    // answer something when my nick is mentioned.
    // temporary OFF as makes some flood
    // _self.sayAnswerIfRequired(from, to, msg);

    // Try to evaluate msg as math string
    let mathResult = _self.evalMath(msg);

    const actions = [
        decodeIfRequired,
        sayWeatherForecastIfRequired,
        _self.sayHelp,
        _self.sayExchangeRateIfRequried,
        _self.saySearchResult,
        _self.processLogCommand,
        _self.lastSeen,
        _self.checkIfImage,
        // _self.mezhBankRate,
        _self.processTodo,
        _self.silence
    ];

    try {
        if (typeof mathResult === "object" || (!isNaN(mathResult))) {
            _self.sayMathAnswer(from, to, msg, mathResult);
        }

        actions.forEach(action => {
            action.call(_self, from, to, msg);
        });
    } catch (e) {
        console.log(e);
        _self.errorMsg(from, to, e);
    }
};

messageHandler.processTodo = (from, to, msg) => {
    const isTodo = msg.indexOf(messageHandler.commands.todo) === 0;

    if (isTodo) {
        todo
            .process(from, to, msg)
            .then((answer) => {
                client.say(to, answer);
                channelLogger.log(client.nick, to, answer);
            })
            .catch((err) => {
                console.log(err);
            });
    }
};

messageHandler.mezhBankRate = function(from, to, msg) {
    const mezhbank = msg.indexOf(messageHandler.commands.mezhbank) === 0;

    if (mezhbank) {
        const answer = interbank.getRates();

        client.say(to, answer);
        channelLogger.log(client.nick, to, answer);
    }
};

messageHandler.checkIfImage = function(from, to, msg) {
    const hasUrl = /((https|http):\/\/[\S]*)/.test(msg);

    if (hasUrl) {
        images.storeImage(msg);
    }
};

messageHandler.lastSeen = function(from, to, msg) {
    const isLastseenCommand = msg.indexOf(messageHandler.commands.lastSeen) === 0;

    if (isLastseenCommand) {
        const nick = msg.split(/\s/)[1];

        seen.lastSeen(nick, text => {
            text = `${from}, ${text}`;
            client.say(to, text);
            channelLogger.log(client.nick, to, text);
        });
    }
};

messageHandler.silence = function(from, to, msg) {
    const isSilenceCommand = msg.indexOf(messageHandler.commands.silence) === 0;

    if (isSilenceCommand) {
        messageHandler.isDecoding[to] = false;

        client.say(to, 'Я закрою свое хлебало на 60 секунд и не буду переводить латинские символы');

        setTimeout(function() {
            delete messageHandler.isDecoding[to];
        }, parseInt(config.silenceTime, 10));
    }
};

messageHandler.processLogCommand = function(from, to, msg) {
    const isLogCommand = msg.indexOf(messageHandler.commands.log) === 0;

    if (isLogCommand) {
        channelLogger.getLogResult(from, to, msg, (logUrl) => {
            const text = `${from}: поищи тут ${logUrl}`;
            client.say(to, text);
            channelLogger.log(client.nick, to, text);
        },
        (msg) => {
            client.say(to, msg);
            channelLogger.log(client.nick, to, msg);
        });
    }
};

messageHandler.saySearchResult = function(from, to, msg) {
    if (msg.indexOf(messageHandler.commands.search) === 0) {

        if (msg.trim() === messageHandler.commands.search) {
            const helpText = channelLogger.help();
            client.say(to, helpText);
            channelLogger.log(client.nick, to, helpText);
        } else {
            const params = channelLogger.getPreparedParams(msg);

            if (params['channel'] && params['channel'] === '*') {
                params.channel = null;
            }

            if (params.search !== null) {
                channelLogger.getSearchResult(params.search, from, params.channel, (url) => {
                    let text;
                    if (url) {
                        text = `${from}, Найденные результаты тут: ${url}`;
                    } else {
                        text = `${from}, а нифига не найдено... попробуй еще`;
                    }

                    client.say(to, text);
                    channelLogger.log(client.nick, to, text);
                });
            } else {
                throw new Error('я не понимаю');
            }
        }

        channelLogger.log(from, to, msg);
    }

};

messageHandler.sayHelp = function(from, to, msg) {
    let isHelpRequested = (msg.indexOf(messageHandler.commands.help) === 0 || msg.indexOf(messageHandler.commands.help2) === 0);

    if (isHelpRequested) {
        let str = '';
        for(let i in this.commands) {
            str += this.commands[i] +' ';
        }

        const text = 'Я понимаю такие комманды: ' + str;

        client.say(to, text);
        channelLogger.log(client.nick, to, text);
    }

    return this;
};

messageHandler.sayExchangeRateIfRequried = function (from, to, msg) {
    if (msg.indexOf(messageHandler.commands.rate) === 0) {
        msg = msg.replace(',', '.'); //the module doesn't understand [,]

        const text = exchanger.getRate(from, msg, client);

        client.say(to, text);
        channelLogger.log(client.nick, to, text);
    }
};

messageHandler.evalMath = function (msg) {
    let result = NaN;

    if (msg[msg.length - 1] === "?") {
        msg = msg.replace(',', '.'); //the module doesn't understand [,]

        try {
            let mathStr = msg.substr(0, msg.length - 1);

            result = math.eval(mathStr);
        } catch (err) {
            // console.error(err);
        }
    }

    return result;
};

messageHandler.sayMathAnswer = function (from, to, msg, answer) {
    let myNick = client.nick;

    let recipient = (myNick !== to) ? to : from;

    client.say(recipient, `${msg} = ${answer}`);
    channelLogger.log(from, to, `${msg} = ${answer}`);
};

/**
 * answer something if nick is mentioned
 */
messageHandler.sayAnswerIfRequired = function (from, to, msg) {
    let myNick = client.nick;
    let answer = getRandomAnswer();

    if (to !== myNick) {
        if (isNickMentioned(myNick, msg)) {
            client.say(to, `${from}, ${answer}`);
            channelLogger.log(client.nick, to, msg);
        }
    } else {
        client.say(from, answer);
        channelLogger.log(client.nick, from, msg);
    }
};


module.exports = messageHandler;