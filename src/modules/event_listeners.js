"use strict";

const client = require('../irc_client');
const joinHandler = require('./join_handler');
const messageHandler = require('./message_handler');
const logger = require('./channel_logger');
const fs = require('fs');
const seen = require('./last_seen');

logger.init();

let listeners = {};

listeners.onRegistered = (msg) => {
    // console.log('-------------- Registered ---------------');
    // console.log(msg);
    // console.log('-----------------------------------------');

};

listeners.onMOTD = (motd) => {
    // console.log('------------- Message of the Day -----------------------');
    // console.log(motd);
    // console.log('\n--------------------------------------------------------');
};

listeners.quit = (nick, reason, channels, msg) => {
    seen.updateRecord(nick, 'QUIT', msg.host, channels[0]);

    channels.forEach((channel) => {
        const ind = client.channel_users[channel].indexOf(nick);
        if (ind > -1) {
            logger.log('', channel, `${nick} has quit the ${channel}`);
            client.channel_users[channel].splice(ind, 1);
        }
    });
};

listeners.nickChanged = (oldNick, newNick, channels, msg) => {
    // console.log(oldNick);
    // console.log(newNick);
    // console.log(client.channels);
    // console.log(channels);
    // console.log(msg);
};

listeners.onAction = (from, to, text, msg) => {
    // console.log(from);
    // console.log(to);
    // console.log(text);
    // console.log(msg);
};

listeners.onRaw = (msg) => {
    try {
        if (msg.command === 'JOIN') {
            seen.updateRecord(msg.nick, 'JOIN', msg.host, msg.args[0]);
            logger.log('', msg.args[0], `${msg.nick} has joined the ${msg.args[0]}`);

            client.channel_users[msg.args[0]].push(msg.nick);
        }

        if (msg.command === 'PART') {
            seen.updateRecord(msg.nick, 'PART', msg.host, msg.args[0]);
            logger.log('', msg.args[0], `${msg.nick} has left the ${msg.args[0]}`);

            const ind = client.channel_users[msg.args[0]].indexOf(msg.nick)
            if (ind > -1) {
                client.channel_users[msg.args[0]].splice(ind, 1);
            }
        }
    } catch (e) {
        fs.appendFile('errors.log', e + "\n", (err) => {});
    }
};

listeners.onJoin = (channel, nick, msg) => {
    joinHandler.coreProcess(channel, nick, msg);
};

listeners.getNamesList = (channel, nicks) => {
    // console.log(client);
    // console.log(channel);
    // console.log(nicks);
};

listeners.onMessage = (from, to, msg) => {
    messageHandler.coreProcess(from, to, msg);
};

listeners.onNames = (channel, nicks) => {
    if (typeof client['channel_users'] === 'undefined') {
        client['channel_users'] = {};
    }

    client['channel_users'][channel] = Object.keys(nicks);
};

module.exports = listeners;