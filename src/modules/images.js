"use strict";

const mysqlConnection = require('./mysql_connect');
const http = require('http');
const https = require('https');
const imageType = require('image-type');
const moment = require('moment');

module.exports = {
    tableName: 'collected_image_urls',

    storeImage: function(msg) {
        const _self = this;
        const matches = msg.match(/((https|http):\/\/[\S]*)/g);

        matches.forEach((url) => {
            const client = this.getHttpClient(url);

            client
                .get(url, res => {
                    res.once('data', chunk => {
                        res.destroy();
                        if (imageType(chunk) !== null) {
                            _self.persistImage(url);
                        }
                    });
                });
        });
    },

    getHttpClient: function(url) {
        return (url.indexOf('https') === 0) ? https : http;
    },

    persistImage: function(url) {
        const now = moment().format('YYYY-MM-DD HH:mm:ss');
        let query = `SELECT count(id) AS count FROM ${this.tableName} WHERE url = "${url}"`;

        mysqlConnection.query(query, (err, result) => {
            if (err) {
                return;
            }

            if (result[0].count === 0) {
                query = `INSERT INTO ${this.tableName} (url, created_at, updated_at) VALUES("${url}", "${now}", "${now}")`;
                mysqlConnection.query(query, (err, result) => {});
            }
        });
    }
};