"use strict";

const fs = require('fs');
const client = require('../irc_client');
const nodemailer = require('nodemailer');
const moment = require('moment');

const Dropbox = require('dropbox');
const dbx = new Dropbox({ accessToken: 'q8PYNEEORZUAAAAAAAACL3NC-rMKxHWHVTsopBwHxdPXSugMtkAtVb9iHZjJ6f-f' });

let Logger = {
    logDirName: './channels-logs',

    help: function() {
        let str = '';

        str += '!поиск "фраза" (параметры)\n';
        str += 'Пример: !поиск "чебурашка" (channel: *, qty: 4)\n';
        str += 'Пример: !поиск "чебурашка" (channel: *)\n';
        str += 'Пример: !поиск "чебурашка"\n';

        return str;
    },

    init: function() {
        if (!fs.existsSync(this.logDirName)) {
            fs.mkdirSync(this.logDirName);
        }
    },

    log: function(from, channel, msg) {
        const now = new Date();
        const monthFormatted = this.get2DigitsMonth(now.getMonth() + 1);
        const dateFormatted = this.get2DigitsMonth(now.getDate());
        const secondsFormatted = this.get2DigitsMonth(now.getSeconds());
        const hoursFormatted = this.get2DigitsMonth(now.getHours());
        const minutesFormatted = this.get2DigitsMonth(now.getMinutes());

        const fullDateFormatted = now.getFullYear() + '-'
            + monthFormatted + '-'
            + dateFormatted + ' '
            + hoursFormatted + ':'
            + minutesFormatted + ':'
            + secondsFormatted;

        const logFileName = this.getLogFileName(channel);

        const logMsg = `${fullDateFormatted}\t${from}:\t${msg}\n`;

        fs.appendFile(logFileName, logMsg, (err) => {
            if (err) {
                console.log(err);
            }
        });
    },

    get2DigitsMonth: function(month) {
        return month < 10 ? `0${month}` : month;
    },

    getLogFileName: function(channelName, date) {
        let now;
        if (!date) {
            now = new Date();
        } else {
            now = new Date(date);
        }

        return this.logDirName + '/'
            + channelName + '-'
            + now.getFullYear() + '-'
            + this.get2DigitsMonth(now.getMonth() + 1) + '-'
            + this.get2DigitsMonth(now.getDate()) + '.log';
    },

    parseParams: function parseParams(str) {
        let paramObject = {};

        let params = /\((.*)\)/.exec(str);
        if (null === params) {
            paramObject.search = '';
            return paramObject;
        }

        params = params[1].split(',');

        for(let i=0, len=params.length; i < len; i++) {
            let paramParts = params[i].split(':');
            paramParts[0] = paramParts[0].trim();
            paramParts[1] = paramParts[1].trim();

            paramObject[paramParts[0]] = paramParts[1];
        }

        return paramObject;
    },

    getPreparedParams: function(msg) {
        let search = /"(.*)"/i.exec(msg);

        let params = this.parseParams(msg);

        if (search) {
            params.search = search[1];
        }

        if (params.channel && params.channel === '*') {
            params.channel = null;
        }

        if (!params.qty) {
            params.qty = 5;
        }

        return params;
    },

    isDateValid: function(date) {
        return moment(date, 'YYYY-MM-DD', true).isValid();
    },

    getFileNamesInPeriod: function(params) {
        if (!params.end) {
            params.end = moment().format('YYYY-MM-DD');
        }

        if (!this.isDateValid(params.start) || !this.isDateValid(params.end)) {
            throw new Error("Date is not valid");
        }

        let counterDate = moment(params.start, 'YYYY-MM-DD', true).subtract(1, 'days');
        const end = moment(params.end, 'YYYY-MM-DD', true);
        const diff = end.diff(counterDate, 'days');

        let fileNames = [];
        for(let i=0; i < diff; i++) {
            fileNames.push(this.getLogFileName(params.channel, counterDate.add(1, 'days')));
        }

        return fileNames;
    },

    getLogsContent: function(logFileNames) {
        let content = '';

        for(let i=0, len=logFileNames.length; i < len; i++) {
            if (fs.existsSync(logFileNames[i])) {
                content += fs.readFileSync(logFileNames[i], {encoding: 'utf8'});
            }
        }

        return content;
    },

    putLogsInCloud: function(logName, content, requestDate, callback, errCallback) {
        dbx.filesUpload({path: logName, contents: content})
        .then(function(response) {
            dbx.sharingCreateSharedLinkWithSettings({path: logName})
                .then((result) => {
                    callback(result.url);
                })
                .catch(result => {
                    if (result.status === 409) {
                        dbx.sharingListSharedLinks()
                            .then(result => {
                                callback(result.url);
                            })
                            .catch(result => {
                                errCallback('');
                            });
                    } else {
                        console.log(result);
                        errCallback('');
                    }

                });
        })
        .catch(function(error) {
            console.error(error);
            errCallback('');
        });
    },

    getLogResult: function(from, to, msg, callback, errCallback) {
        const requestDate = moment();
        const params = this.getPreparedParams(msg);
        params.channel = params.channel || to;
        let sayResult = '';

        if (params.start) {
            const fileNames = this.getFileNamesInPeriod(params);
            const logsContent = this.getLogsContent(fileNames);

            const logFileName = `/logs/request-from-${from}-${requestDate.format('YYYY-MM-DD HH:mm:ss')}.log`;
            this.putLogsInCloud(logFileName, logsContent, requestDate, callback, errCallback);
        } else {
            sayResult = "Обязательный параметр start не указан\n";
            sayResult += "Format:  !лог (start: 2017-10-01, end: 2017-10-03, channel: #autoua)\n";

            errCallback(sayResult);
        }
    },

    sendEmail: function(params, logFileName) {
        fs.access(logFileName, fs.constants.R_OK, function(err) {
            if (err) {
                return err;
            }

            fs.readFile(logFileName, 'utf8', function (err, data) {
                if (err) {
                    return console.log(err);
                }

                let transporter = nodemailer.createTransport({
                    host: 'robots.1gb.ua',
                    port: 25,
                    secure: false, // true for 465, false for other ports
                });

                let mailOptions = {
                    from: client.opt.sendEmailAddress,
                    to: params.email,
                    subject: `Логи канала ${params.channel} за ${params.period}`,
                    text: data
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message sent: %s', info.messageId );
                });
            });
        });
    },

    getSearchResult: function (search, from, channel=null, callback) {
        const requestDate = moment();
        let files = this.getListOfFiles(channel);
        let found = [];

        for(let f in files) {
            let content = this.getFileContent(files[f]);

            let strings = content.split("\n");
            for(let i in strings) {
                if (strings[i].indexOf(search) >= 0) {
                    found.push(files[f] + ': ' + strings[i]);
                }
            }
        }

        const foundContent = found.join('\n');
        const logFileName = `/logs/search-request-from-${from}-${requestDate.format('YYYY-MM-DD HH:mm:ss')}.log`;

        if (found.length > 0) {
            this.putLogsInCloud(logFileName, foundContent, requestDate, callback);
        } else {
            callback(false);
        }

    },

    getFileContent: function(fileName) {
        return fs.readFileSync(this.logDirName + '/' + fileName, 'utf8');
    },

    getListOfFiles: function(channel) {
        let items = fs.readdirSync(this.logDirName);
        let files = [];

        for (let i=0, len=items.length; i < len; i++) {
            let item = items[i].split('-', 1)[0];

            if (channel) {
                if (channel === item) {
                    files.push(items[i]);
                }
            } else {
                files.push(items[i]);
            }
        }

        return files;
    }
};

module.exports = Logger;