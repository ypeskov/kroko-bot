let client = require('../irc_client');
let greetings = require('./join_greetings');

function greetNickInChannel(nick, channel) {
    let randomGreetingIndex = Math.floor(Math.random() * greetings.greetings_personal.length);
    let greeting = greetings.greetings_personal[randomGreetingIndex];

    setTimeout(() => {
        client.say(channel, `${greeting} ${nick}`);
    }, 3000);

}

/**
 * Say something into channel when joining this channel
 *
 * @param {any} channel
 */
function greetAllInChannel(channel) {
    let randomGreetingIndex = Math.floor(Math.random() * greetings.greetings_all.length);
    let greeting = greetings.greetings_all[randomGreetingIndex];

    setTimeout(() => {
        client.say(channel, greeting);
    }, 2000);
}


let joinHandler = {};

joinHandler.coreProcess = function (channel, nick, msg) {
    let myNick = client.nick;

    //greet all if join myself or greet a new nick
    if (nick !== myNick) {
        greetNickInChannel(nick, channel);
    } else {
        greetAllInChannel(channel);
    }
};


module.exports = joinHandler;