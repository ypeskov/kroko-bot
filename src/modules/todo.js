'use strict';

const mysqlConnection = require('./mysql_connect');
const moment = require('moment');

const tableName = 'todos';

const Todo = {
    commands: [
        'add',
        'list',
        'done',
        'done-list'
    ],

    parseMsg(msg) {
        const words = msg.split(/\s/);
        const command = words[1] || null;
        const text = words.slice(2).join(' ');


        return [command, text];
    },

    isCommand(command) {
        return this.commands.indexOf(command) > -1;
    },

    getHelpMsg() {
        let helpMsg = `format: !todo [command] [text]\n`;
        helpMsg += `commands: ` + this.commands.join('; ');

        return helpMsg;
    },

    addTodo(description, author, time) {
        return new Promise((resolve, reject) => {
            if (description.length === 0) {
                resolve(`${from}, error: todo text must not be empty`);
            }

            let sql = `INSERT INTO todos (description, author, done, who_done, created_at, updated_at) `;
            sql += `VALUES('${description}', '${author}', 0, '', '${time.format('YYYY-MM-DD HH:mm:ss')}', '${time.format('YYYY-MM-DD HH:mm:ss')}')`;

            mysqlConnection.query(sql, (err, result) => {
                if (err) {
                    console.log(err);
                    reject(new Error('Error while adding todo'));
                }

                resolve(`${author}, your TODO is stored`);
            });
        });
    },

    listTodo() {
        return new Promise((resolve, reject) => {
            const sql = `SELECT * FROM ${tableName} WHERE \`done\` = 0`;


            mysqlConnection.query(sql, (err, result) => {
                if (err) {
                    reject(new Error('error while adding todo'));
                }

                let todos = '';
                result.forEach((val) => {
                    todos += `${val.id}: ${val.description} (${val.author} at ${moment(val.created_at).format('YYYY-MM-DD HH:mm:ss')})\n`;
                });

                resolve(`TODO:\n${todos}`);
            });
        });
    },

    doneList() {
        return new Promise((resolve, reject) => {
            const sql = `SELECT * FROM ${tableName} WHERE \`done\` = 1`;

            mysqlConnection.query(sql, (err, result) => {
                if (err) {
                    reject(new Error('error while adding todo'));
                }

                let todos = '';
                result.forEach((val) => {
                    todos += `${val.id}: [${val.description}] marked as done `;
                    todos += `at ${moment(val.created_at).format('YYYY-MM-DD HH:mm:ss')} by ${val.who_done}\n`;
                });

                resolve(`DONE items:\n${todos}`);
            });
        });
    },

    done(from, msg) {
        return new Promise((resolve, reject) => {
            const id = parseInt(msg, 10);

            if (isNaN(id)) {
                resolve('format:  !todo done [number]');
                return;
            }

            const now = moment().format('YYYY-MM-DD HH:mm:ss');

            const sql = `UPDATE ${tableName} SET \`done\`=1, \`when_done\`="${now}", \`who_done\`="${from}" WHERE \`id\`=${id}`;

            mysqlConnection.query(sql, (err, res) => {
                if (err) { resolve('Error while making done TODO ' + id); }

                if (res.affectedRows > 0) {
                    resolve(`TODO ${id} is marked as DONE`);
                } else {
                    resolve('якась хуйня трапылась');
                }
            });
        });
    },

    process(from, to, msg) {
        const parsedMsg = this.parseMsg(msg);
        const now = moment();

        return new Promise((resolve, reject) => {
            if (parsedMsg[0] === null) {
                resolve(this.getHelpMsg());
            }

            if (!this.isCommand(parsedMsg[0])) {
                resolve('Unknown command\n' + this.getHelpMsg());
            }

            const command = parsedMsg[0];
            const todo = parsedMsg[1];

            switch(command) {
                case 'add':
                    this
                        .addTodo(todo, from, now)
                        .then((text) => {
                            resolve(text);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                    break;
                case 'list':
                    this
                        .listTodo()
                        .then((text) => {
                            resolve(text);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                    break;
                case 'done':
                    this
                        .done(from, todo)
                        .then((text) => {
                            resolve(text);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                    break;
                case 'done-list':
                    this
                        .doneList()
                        .then((text) => {
                            resolve(text);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                    break;
                default:
                    resolve('Error\n' + this.getHelpMsg());
            }
        });
    }
};

module.exports = Todo;