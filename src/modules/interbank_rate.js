'use strict';

const request = require('request');
const moment = require('moment');



const interbank = {
    sourceUrl: 'http://openrates.in.ua/rates',

    rates: [],

    lastUpdated: null,

    init() {
        this.updateRates();

        setInterval(() => {
            this.updateRates();
        }, 30 * 60 * 1000);
    },

    updateRates() {
        request(this.sourceUrl, (err, res, body) => {
            if (err) {
                return false;
            }

            try {
              this.rates = JSON.parse(body);
            } catch (e) {
              // console.log(e);
            }


            this.lastUpdated = moment();
        });
    },

    getRates() {
        let answer = `USD. Межбанк. Покупка: ${this.rates.USD.interbank.buy} Продажа: ${this.rates.USD.interbank.sell}\n`;
        answer += `USD. НБУ: ${this.rates.USD.nbu.buy}\n`;
        answer += `EUR. Межбанк. Покупка: ${this.rates.EUR.interbank.buy} Продажа: ${this.rates.EUR.interbank.sell}\n`;
        answer += `EUR. НБУ: ${this.rates.EUR.nbu.buy}\n`;

        return answer;
    }
};

module.exports = interbank;