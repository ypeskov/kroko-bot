"use strict";

const oxr = require('open-exchange-rates');
const fx = require('money');
const moment = require('moment');

oxr.set({ app_id: 'e6c677f633df42648ecc217251515f04' });

let exchanger = {
    timeUpdated: null,

    fx: fx,

    init: function() {
        this.updateRates();
    },

    updateRates: function() {
        oxr.latest(function() {
            this.fx.rates = oxr.rates;
            this.fx.base = oxr.base;
            this.timeUpdated = moment();
        }.bind(this));

        return this;
    },

    /**
     *
     * @return {string}
     */
    getCurrenciesList: function() {
        let str = '';

        for (let i in this.fx.rates) {
            str += `${i}:${this.fx.rates[i]}    `;
        }

        return str;
    },

    /**
     *
     * @param command
     * @returns {string}
     */
    getHelpMsg: function(command) {
        let str = '';
        str += `Формат: ${command} 50 currency1 currency2\n`;
        str += `Список валют: ${command} список`;

        return str;
    },

    /**
     *
     * @param {int} amount
     * @param {string} from
     * @param {string} to
     * @return int
     */
    calculateAmount: function(amount, from, to) {
        let now = moment();

        //if last update is more than 60 minutes ago
        if (now.diff(this.timeUpdated)/1000/60 > 60) {
            this.updateRates();
        }

        return this.fx(amount).from(from).to(to);
    },

    convertStringToCode: function(str) {
        let strInLower = str.toLowerCase();

        let foundKey = null;
        Object.keys(this.currencyLabels).some((key) => {
            let isKey = strInLower.indexOf(key) >= 0;

            if (isKey) {
                foundKey = key;
            }

            return isKey;
        });

        if (foundKey) {
            return this.currencyLabels[foundKey];
        } else {
            return str;
        }
    },

    getRate: function(from, msg, client) {
        let params = msg.split(/\s+/);

        if (!params[1]) {
            return exchanger.getHelpMsg('!обмен');
        }

        if (params[1] === 'список') {
            client.say(from, exchanger.getCurrenciesList());
            return `${from}, список доступных курсов ищи в привате у себя.`
        }

        let originAmount = parseFloat(params[1]);
        let currencyOrigin = exchanger.convertStringToCode(params[2]).toUpperCase();
        let currencyResult = exchanger.convertStringToCode(params[3]).toUpperCase();
        let amount, text;

        if (!isNaN(originAmount)) {
            amount = exchanger.calculateAmount(originAmount, currencyOrigin, currencyResult).toFixed(2);
            text = `${originAmount} ${currencyOrigin} = ` + `${amount} ${currencyResult}`
        } else {
            text = `${from}, ${params[1]} не є числом. Спробуйте ще, будь-ласка`;
        }

        return text;
    },

    currencyLabels: {
        'гривн': 'UAH',
        'грн': 'UAH',
        'дол': 'USD',
        'бакс': 'USD',
        '$': 'USD',
        'евр': 'EUR',
        'злот': 'PLN',
        'фунт': 'GBP'
    }
};


module.exports = exchanger;

