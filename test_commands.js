"use strict";

// const handler = require('./src/modules/message_handler');
const weather = require('./src/modules/weather_underground');
const moment = require('moment');

const now = moment().format('YYYY-MM-DD HH:mm:ss');

// handler.processTodo('aa', 'aa', `!todo add Test test barmaley ${now}`);
// handler.processTodo('aa', 'aa', '!todo add');
// handler.processTodo('aa', 'aa', '!погода Одесса');

weather.getParametersFromMsg('yura', '#autoua', '!погода краков', '');
weather.getParametersFromMsg('yura', '#autoua', '!погода odessa', '');
weather.getParametersFromMsg('yura', '#autoua', '!погода odessa in germany', '');